(function(){
	var $subNav = document.getElementsByClassName('js-l-sub-nav')[0];
	var $navContent = document.getElementsByClassName('m-nav__content')[0];
	var link = document.getElementsByTagName('link')[0];

	if(window.navigator.userAgent.indexOf('Trident/') > 0)  {
		link.insertAdjacentHTML('beforebegin','<link rel="stylesheet" type="text/css" href="node_modules/swiper-v4/dist/css/swiper.min.css" />');	
	} else {
		link.insertAdjacentHTML('beforebegin','<link rel="stylesheet" type="text/css" href="node_modules/swiper/swiper-bundle.min.css" />');
	}
	
	document.getElementsByClassName('js-toggle-nav')[0].onclick = function(e) {
		e.preventDefault();
		$navContent.style.left = '0';
	};

	document.getElementsByClassName('js-sub-nav')[0].onmouseenter = function() {
		$subNav.classList.remove('hidden');
	};

	$subNav.onmouseleave = function() {
		$subNav.classList.add('hidden');	
	};

	document.getElementsByTagName('main')[0].onclick = function() {
		if(typeof $navContent !== 'undefined') {
			if($navContent.style.left === '0px') {
				$navContent.style.left = '-310px';
			}
		}
	};

	var swiper = new Swiper('.swiper-container', {
    	navigation: {
      		nextEl: '.swiper-button-next',
      		prevEl: '.swiper-button-prev',
    	},
	});
})();